/*				A-Star.cpp				*/
/*	 http://en.wikipedia.org/wiki/A*	*/
/*		Compiler: Dev-C++ 4.9.9.2		*/
/*			  FB - 201012256			*/

#include <iostream>
#include <iomanip>
#include <queue>
#include <string>
#include <math.h>
#include <ctime>
using namespace std;

const int sizeX = 60;						// horizontal size of the map
const int sizeY = 60;						// vertical size size of the map
static int map[sizeX][sizeY];				// A 2D array of the map, using the vertical and horizontal constraints
static int closed_nodes_map[sizeX][sizeY];	// map of closed (tried-out) nodes
static int open_nodes_map[sizeX][sizeY];	// map of open (not-yet-tried) nodes
static int dir_map[sizeX][sizeY];			// map of directions
const int dir = 8;							// number of possible directions to go at any position
											// if dir == 4
											//static int dx[dir] = {1, 0, -1, 0};
											//static int dy[dir] = {0, 1, 0, -1};
											// if dir == 8
static int dx[dir] = { 1, 1, 0, -1, -1, -1, 0, 1 };
static int dy[dir] = { 0, 1, 1, 1, 0, -1, -1, -1 };

class node
{
	// current position
	int xPos;
	int yPos;

	// total distance already travelled to reach the node
	int level;

	// priority = level + remaining distance estimate
	int priority;  // smaller: higher priority

public:

	// Constructor
	node(int xp, int yp, int lv, int pr)
	{
		xPos = xp;			// Assigning the parameter, xp, to xPos
		yPos = yp;			// Assigning the parameter, yp, to yPos
		level = lv;			// Assigning the parameter, lv, to level
		priority = pr;		// Assigning the parameter, pr, to priority
	}

	// Gets the x position and returns it as a variable
	int getxPos() const
	{
		return xPos;
	}

	// Gets the y position and returns it as a variable
	int getyPos() const
	{
		return yPos;
	}

	// Gets the level and returns it as a variable
	int getLevel() const
	{
		return level;
	}

	// Gets the priority and returns it as a variable
	int getPriority() const
	{
		return priority;
	}

	// Adds the level and estimated remaining difference to the goal, and assigns it to priority
	void updatePriority(const int & xDest, const int & yDest)
	{
		// Part of the main A* algorithm
		priority = level + estimate(xDest, yDest) * 10; // A*			
	}

	// give better priority to going straight instead of diagonally
	void nextLevel(const int & iDirection)
	{
		level += (dir == 8 ? (iDirection % 2 == 0 ? 10 : 14) : 10);
	}

	// Estimation function for the remaining distance to the goal.
	const int & estimate(const int & xDest, const int & yDest) const
	{
		static int xDist, yDist, dist;
		xDist = xDest - xPos;
		yDist = yDest - yPos;

		// Euclidian Distance
		dist = static_cast<int>(sqrt(xDist * xDist + yDist * yDist));

		// Manhattan distance
		//dist = abs(xDist) + abs(yDist);

		// Chebyshev distance
		//dist = max(abs(xDist), abs(yDist));

		return(dist);
	}
};

// Determine priority (in the priority queue)
bool operator<(const node & aPriority, const node & bPriority)
{
	return aPriority.getPriority() > bPriority.getPriority();
}

// A-star algorithm.
// The route returned is a string of direction digits.
string pathFind(const int & xStart, const int & yStart, const int & xFinish, const int & yFinish)
{
	static priority_queue<node> priorityQueue[2]; // list of open (not-yet-tried) nodes
	static int priorityQueueIndex;
	static node* sizeX0;
	static node* sizeY0;
	static int i, j, x, y, xdx, ydy;
	static char c;
	priorityQueueIndex = 0;

	// reset the node maps
	for (y = 0; y < sizeY; y++)
	{
		for (x = 0; x < sizeX; x++)
		{
			closed_nodes_map[x][y] = 0;
			open_nodes_map[x][y] = 0;
		}
	}

	// create the start node and push into list of open nodes
	sizeX0 = new node(xStart, yStart, 0, 0);
	sizeX0->updatePriority(xFinish, yFinish);
	priorityQueue[priorityQueueIndex].push(*sizeX0);
	open_nodes_map[x][y] = sizeX0->getPriority(); // mark it on the open nodes map

												  // A* search
	while (!priorityQueue[priorityQueueIndex].empty())
	{
		// get the current node w/ the highest priority
		// from the list of open nodes
		sizeX0 = new node(priorityQueue[priorityQueueIndex].top().getxPos(), priorityQueue[priorityQueueIndex].top().getyPos(),
			priorityQueue[priorityQueueIndex].top().getLevel(), priorityQueue[priorityQueueIndex].top().getPriority());

		x = sizeX0->getxPos(); y = sizeX0->getyPos();

		priorityQueue[priorityQueueIndex].pop(); // remove the node from the open list
		open_nodes_map[x][y] = 0;
		// mark it on the closed nodes map
		closed_nodes_map[x][y] = 1;

		// quit searching when the goal state is reached
		//if((*sizeX0).estimate(xFinish, yFinish) == 0)
		if (x == xFinish && y == yFinish)
		{
			// generate the path from finish to start
			// by following the directions
			string path = "";
			while (!(x == xStart && y == yStart))
			{
				j = dir_map[x][y];
				c = '0' + (j + dir / 2) % dir;
				path = c + path;
				x += dx[j];
				y += dy[j];
			}

			// garbage collection
			delete sizeX0;
			// empty the leftover nodes
			while (!priorityQueue[priorityQueueIndex].empty())
			{
				priorityQueue[priorityQueueIndex].pop();
			}
			return path;
		}

		// generate moves (child nodes) in all possible directions
		for (i = 0; i < dir; i++)
		{
			xdx = x + dx[i]; ydy = y + dy[i];

			if (!(xdx < 0 || xdx > sizeX - 1 || ydy < 0 || ydy > sizeY - 1 || map[xdx][ydy] == 1 || closed_nodes_map[xdx][ydy] == 1))
			{
				// generate a child node
				sizeY0 = new node(xdx, ydy, sizeX0->getLevel(),
					sizeX0->getPriority());
				sizeY0->nextLevel(i);
				sizeY0->updatePriority(xFinish, yFinish);

				// if it is not in the open list then add into that
				if (open_nodes_map[xdx][ydy] == 0)
				{
					open_nodes_map[xdx][ydy] = sizeY0->getPriority();
					priorityQueue[priorityQueueIndex].push(*sizeY0);
					// mark its parent node direction
					dir_map[xdx][ydy] = (i + dir / 2) % dir;
				}
				else if (open_nodes_map[xdx][ydy] > sizeY0->getPriority())
				{
					// update the priority info
					open_nodes_map[xdx][ydy] = sizeY0->getPriority();
					// update the parent direction info
					dir_map[xdx][ydy] = (i + dir / 2) % dir;

					// replace the node
					// by emptying one priorityQueue to the other one
					// except the node to be replaced will be ignored
					// and the new node will be pushed in instead
					while (!(priorityQueue[priorityQueueIndex].top().getxPos() == xdx && priorityQueue[priorityQueueIndex].top().getyPos() == ydy))
					{
						priorityQueue[1 - priorityQueueIndex].push(priorityQueue[priorityQueueIndex].top());
						priorityQueue[priorityQueueIndex].pop();
					}
					priorityQueue[priorityQueueIndex].pop(); // remove the wanted node

															 // empty the larger size priorityQueue to the smaller one
					if (priorityQueue[priorityQueueIndex].size() > priorityQueue[1 - priorityQueueIndex].size())
					{
						priorityQueueIndex = 1 - priorityQueueIndex;
					}
					while (!priorityQueue[priorityQueueIndex].empty())
					{
						priorityQueue[1 - priorityQueueIndex].push(priorityQueue[priorityQueueIndex].top());
						priorityQueue[priorityQueueIndex].pop();
					}
					priorityQueueIndex = 1 - priorityQueueIndex;
					priorityQueue[priorityQueueIndex].push(*sizeY0); // add the better node instead
				}
				else
				{
					delete sizeY0; // garbage collection
				}
			}
		}
		delete sizeX0; // garbage collection
	}
	return ""; // no route found
}

int main()
{
	srand(time(NULL));

	// create empty map
	for (int y = 0; y < sizeY; y++)
	{
		for (int x = 0; x < sizeX; x++) map[x][y] = 0;
	}

	// fillout the map matrix with a '+' pattern
	for (int x = sizeX / 8; x < sizeX * 7 / 8; x++)
	{
		map[x][sizeY / 2] = 1;
	}

	for (int y = sizeY / 8; y < sizeY * 7 / 8; y++)
	{
		map[sizeX / 2][y] = 1;
	}

	// randomly select start and finish locations
	int xStart, yStart, xFinish, yFinish;
	switch (rand() % 8)
	{
	case 0: xStart = 0; yStart = 0; xFinish = sizeX - 1; yFinish = sizeY - 1;
		break;
	case 1: xStart = 0; yStart = sizeY - 1; xFinish = sizeX - 1; yFinish = 0;
		break;
	case 2: xStart = sizeX / 2 - 1; yStart = sizeY / 2 - 1; xFinish = sizeX / 2 + 1; yFinish = sizeY / 2 + 1;
		break;
	case 3: xStart = sizeX / 2 - 1; yStart = sizeY / 2 + 1; xFinish = sizeX / 2 + 1; yFinish = sizeY / 2 - 1;
		break;
	case 4: xStart = sizeX / 2 - 1; yStart = 0; xFinish = sizeX / 2 + 1; yFinish = sizeY - 1;
		break;
	case 5: xStart = sizeX / 2 + 1; yStart = sizeY - 1; xFinish = sizeX / 2 - 1; yFinish = 0;
		break;
	case 6: xStart = 0; yStart = sizeY / 2 - 1; xFinish = sizeX - 1; yFinish = sizeY / 2 + 1;
		break;
	case 7: xStart = sizeX - 1; yStart = sizeY / 2 + 1; xFinish = 0; yFinish = sizeY / 2 - 1;
		break;
	}

	// Output the map size
	cout << "Map Size (X,Y): " << sizeX << "," << sizeY << endl;

	// Output the start x and y coordinates
	cout << "Start: " << xStart << "," << yStart << endl;

	// Output the finish x and y coordinates
	cout << "Finish: " << xFinish << "," << yFinish << endl;

	// get the route
	clock_t start = clock();
	string route = pathFind(xStart, yStart, xFinish, yFinish);
	if (route == "") cout << "An empty route generated!" << endl;
	clock_t end = clock();
	double time_elapsed = double(end - start);
	cout << "Time to calculate the route (ms): " << time_elapsed << endl;
	cout << "Route:" << endl;
	cout << route << endl << endl;

	// follow the route on the map and display it 
	if (route.length() > 0)
	{
		int j;
		char c;
		int x = xStart;		// Starting x coordinate
		int y = yStart;		// Starting y coordinate
		map[x][y] = 2;		// 2D array using x and y as constraints

		for (int i = 0; i < route.length(); i++)
		{
			c = route.at(i);
			j = atoi(&c);
			x = x + dx[j];
			y = y + dy[j];
			map[x][y] = 3;
		}

		map[x][y] = 4;

		// display the map with the route
		for (int y = 0; y < sizeY; y++)
		{
			for (int x = 0; x < sizeX; x++)
			{
				if (map[x][y] == 0)
				{
					cout << ".";
				}
				else if (map[x][y] == 1)
				{
					cout << "O"; //obstacle
				}
				else if (map[x][y] == 2)
				{
					cout << "S"; //start
				}
				else if (map[x][y] == 3)
				{
					cout << "R"; //route
				}
				else if (map[x][y] == 4)
				{
					cout << "F"; //finish 
				}
			}

			cout << endl;
		}
	}

	getchar(); // wait for a (Enter) keypress  
	return(0);
}
