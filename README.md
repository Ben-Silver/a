This is an A* (A-Star) pathfinding algorithm, found on http://code.activestate.com/recipes/577457-a-star-shortest-path-algorithm/
and refactored in order to make it easier to understand.